const MessageLib = require('./Message');
module.exports = (me) => {
  me.on_connected_to_bridge = () => { }
  me.on_connection_to_bridge_failed = () => { }
  me.on_connected_to_bridge_before = () => { }
  me.on_db_initialized = () => { }
  me.on_db_initialization_got_error = () => { }
  me.on_message_recieved = (type, body, contact) => {
    console.log('teeeest');
    switch (type) {
      case 'text':
        me.on_text_message_recieved(body, contact);
        break;
      case 'image':
        console.log(MessageLib.toFile(body, 'file.png'));
        // me.on_image_message_recieved(body, contact);
        break;
      default:
        console.log(`got message (${type}).`);
        break;
    }
  }
  me.on_anonymouse_message_recieved = (type, body, public_key) => {
    switch (type) {
      case 'text':
        me.on_anonymouse_text_message_recieved(body, public_key);
        break;
      case 'image':
        console.log(MessageLib.toFile(body, 'file.png'));
        // me.on_anonymouse_image_message_recieved(body, public_key);
        break;
      default:
        console.log(`got message (${type}).`);
        break;
    }
  }
  me.on_image_message_recieved = (image, contact) => { };
  me.on_anonymouse_image_message_recieved = (image, public_key) => { };
  me.on_text_message_recieved = (text, contact) => { };
  me.on_anonymouse_text_message_recieved = (text, public_key) => { };
}
