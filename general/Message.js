const NodeRSA = require('node-rsa');
const CHUNK_SIZE = 1024 * 8 * 2;
const keys = [];
/**
 * @author mr-exception
 * @param {string} public_key
 * @return {NodeRSA}
 */
const getPublicKey = key => {
  for (let i = 0; i < keys.length; i++) {
    if (keys[i].key === key) {
      console.log('found node!');
      return keys[i].node;
    }
  }
  const node = new NodeRSA(`-----BEGIN PUBLIC KEY-----${key}-----END PUBLIC KEY-----`);
  keys.push({
    key, node
  });
  console.log('saved node.');
  console.log(keys);
  return node;
}
/**
 * @author mr-exception
 * @param {string} private_key
 * @return {NodeRSA}
 */
const getPrivateKey = key => {
  for (let i = 0; i < keys.length; i++) {
    if (keys[i].key === key) {
      console.log('found node!');
      return keys[i].node;
    }
  }
  const node = new NodeRSA(`-----BEGIN PRIVATE KEY-----${key}-----END PRIVATE KEY-----`);
  keys.push({
    key, node
  });
  console.log('saved node.');
  console.log(keys);
  return node;
}
/**
 * @author mr-exception
 * @param {string} value 
 */
const convertToChunks = value => {
  const results = [];
  for (let i = 0; i < value.length; i += CHUNK_SIZE) {
    results.push(value.substr(i, CHUNK_SIZE));
  }
  return results;
}
/**
 * @author mr-exceptions
 * @param {Channel} channel 
 * @param {Buffer} content 
 * @param {String} type 
 * @return {String}
 */
module.exports.createMessage = (sender_node, dst_public_key, body, type) => {
  let spk = getPrivateKey(sender_node.private_key);
  let dpk = getPublicKey(dst_public_key);
  const content = {
    body, type,
    pk: sender_node.public_key,
    sign: spk.sign({ body, type }, 'base64', 'base64'),
  };
  const cipher = dpk.encrypt(content, 'base64');
  const chunks = convertToChunks(cipher);
  return chunks;
}
/**
 * @author mr-exception
 * @param {array} chunks
 * @return {object}
 */
module.exports.convertChunksToMessage = (src_private_key, chunks) => {
  let rpk = getPrivateKey(src_private_key);
  let cipher = chunks.join('');
  const { body, sign, type, pk } = JSON.parse(rpk.decrypt(cipher, 'utf8').toString());
  let spk = getPublicKey(pk);
  if (spk.verify({ body, type }, sign, 'base64', 'base64')) {
    return { body, type, sender_public_key: pk };
  }
}
/**
 * @author mr-exception
 * @param {File} file
 */
module.exports.toBase64 = (file, resolve, reject) => {
  const reader = new FileReader();
  reader.readAsDataURL(file);
  reader.onload = () => resolve(JSON.stringify({ name: file.name, data: reader.result }));
  reader.onerror = error => reject(error);
}
module.exports.toFile = (content) => {
  const { name, data } = JSON.parse(content);
  let arr = data.split(',');
  let mime = arr[0].match(/:(.*?);/)[1];
  let bstr = atob(arr[1]);
  let n = bstr.length;
  let u8arr = new Uint8Array(n);
  while (n--) {
    u8arr[n] = bstr.charCodeAt(n);
  }
  return new File([u8arr], name, { type: mime });
}