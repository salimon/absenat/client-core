module.exports = class DB {
  constructor() {
    this.db = null;
  }
  checkBrowser() {
    return ('indexedDB' in window);
  }
  async init(public_key, on_success, on_failure) {
    const dbCreateRequest = await window.indexedDB.open(`abs-${public_key}`, 1);
    dbCreateRequest.onupgradeneeded = res => {
      console.log('upgrading indexDB.');
      this.db = res.target.result;
      this.db.createObjectStore('contacts', { keyPath: 'public_key' });
      this.db.createObjectStore('messages', { keyPath: 'uuid' });
      this.db.createObjectStore('bridges', { keyPath: 'public_key' });
      this.db.createObjectStore('chunks', { keyPath: 'uuidindex' });
    }
    dbCreateRequest.onsuccess = res => {
      this.db = res.target.result;
      on_success();
    };
    dbCreateRequest.onerror = res => {
      on_failure(res);
    }
  }
  createBridge(public_key, nickname, ip, port, on_success, on_failure) {
    const transaction = this.db.transaction('bridges', 'readwrite');
    const bridges = transaction.objectStore('bridges');
    const cr = bridges.add({
      public_key, nickname, ip, port,
      last_time_connected: Date.now(),
    });
    cr.onsuccess = () => {
      on_success();
    };
    cr.onerror = res => {
      if (res.target.error.name === 'ConstraintError') {
        const transaction = this.db.transaction('bridges', 'readwrite');
        const bridges = transaction.objectStore('bridges');
        const ur = bridges.put({
          public_key, nickname, ip, port,
          last_time_connected: Date.now(),
        });
        ur.onsuccess = () => {
          on_success();
        };
        ur.onerror = res => {
          on_failure(res)
        };
      } else {
        on_failure(res);
      }
    };
  }
  updateBridgeLastTimeConnected(public_key, on_success, on_failure) {
    this.getBridge(public_key, bridge => {
      const transaction = this.db.transaction('bridges', 'readwrite');
      const bridges = transaction.objectStore('bridges');
      bridge.last_time_connected = Date.now();
      const ur = bridges.put(bridge);
      ur.onsuccess = () => {
        on_success();
      };
      ur.onerror = res => {
        on_failure(res)
      };
    }, () => {
      return on_failure();
    });
  }
  deleteBridge(public_key, on_success, on_failure) {
    const transaction = this.db.transaction('bridges', 'readwrite');
    const bridges = transaction.objectStore('bridges');
    const dr = bridges.delete(public_key);
    dr.onsuccess = () => {
      on_success();
    }
    dr.onerror = res => {
      on_failure(res);
    }
  }
  getBridge(public_key, on_found, on_not_found) {
    const transaction = this.db.transaction('bridges', 'readwrite');
    const bridges = transaction.objectStore('bridges');
    const dr = bridges.get(public_key);
    dr.onsuccess = res => {
      const bridge = res.target.result;
      if (bridge)
        on_found(bridge);
      else
        on_not_found();
    }
    dr.onerror = () => {
      on_not_found();
    }
  }
  getAllBridges(on_found, on_not_found) {
    const transaction = this.db.transaction('bridges', 'readwrite');
    const bridges = transaction.objectStore('bridges');
    const dr = bridges.getAll();
    dr.onsuccess = res => {
      on_found(res.target.result);
    }
    dr.onerror = () => {
      on_not_found();
    }
  }
  createContact(public_key, nickname, bridge_public_key, on_success, on_failure) {
    const transaction = this.db.transaction('contacts', 'readwrite');
    const contacts = transaction.objectStore('contacts');
    const cr = contacts.add({
      public_key, nickname, bridges: [bridge_public_key],
      last_time_connected: Date.now(),
    });
    cr.onsuccess = () => {
      on_success();
    };
    cr.onerror = res => {
      if (res.target.error.name === 'ConstraintError') {
        this.getContact(public_key, contact => {
          const bridges = contact.bridges;
          if (bridges.indexOf(bridge_public_key) === -1)
            bridges.push(bridge_public_key);

          const transaction = this.db.transaction('contacts', 'readwrite');
          const contacts = transaction.objectStore('contacts');
          const ur = contacts.put({
            public_key, nickname, bridges,
            last_time_connected: Date.now(),
          });
          ur.onsuccess = () => {
            on_success();
          };
          ur.onerror = res => {
            on_failure(res)
          };
        }, () => { });
      } else {
        on_failure(res);
      }
    };
  }
  updateContactLastTimeConnected(public_key, on_success, on_failure) {
    this.getContact(public_key, contact => {
      const transaction = this.db.transaction('contacts', 'readwrite');
      const contacts = transaction.objectStore('contacts');
      contact.last_time_connected = Date.now();
      const ur = contacts.put(contact);
      ur.onsuccess = () => {
        on_success();
      };
      ur.onerror = res => {
        on_failure(res)
      };
    }, () => {
      return on_failure();
    });
  }
  deleteContact(public_key, on_success, on_failure) {
    const transaction = this.db.transaction('contacts', 'readwrite');
    const contacts = transaction.objectStore('contacts');
    const dr = contacts.delete(public_key);
    dr.onsuccess = () => {
      on_success();
    }
    dr.onerror = res => {
      on_failure(res);
    }
  }
  getContact(public_key, on_found, on_not_found) {
    const transaction = this.db.transaction('contacts', 'readwrite');
    const contacts = transaction.objectStore('contacts');
    const dr = contacts.get(public_key);
    dr.onsuccess = res => {
      const contact = res.target.result;
      if (contact)
        on_found(contact);
      else
        on_not_found();
    }
    dr.onerror = () => {
      on_not_found();
    }
  }
  getAllContacts(on_found, on_not_found) {
    const transaction = this.db.transaction('contacts', 'readwrite');
    const contacts = transaction.objectStore('contacts');
    const dr = contacts.getAll();
    dr.onsuccess = res => {
      on_found(res.target.result);
    }
    dr.onerror = () => {
      on_not_found();
    }
  }
  storeChunk(id, index, chunk, count, dst_public_key, on_success, on_failure) {
    const transaction = this.db.transaction('chunks', 'readwrite');
    const chunks = transaction.objectStore('chunks');
    const cr = chunks.add({ uuidindex: `${id}-${index}`, chunk, dst_public_key, index, id, count, delivered: false, delivered_at: 0, recieved_at: Date.now() });
    cr.onsuccess = () => {
      on_success();
    };
    cr.onerror = res => {
      on_failure(res);
    };
  }
  getAllChunksByPublicKey(public_key, on_success) {
    const transaction = this.db.transaction('chunks', 'readonly');
    const chunksObjectStore = transaction.objectStore('chunks');
    const request = chunksObjectStore.openCursor();
    const chunks = [];
    request.onsuccess = event => {
      const cursor = event.target.result;
      if (cursor) {
        if (cursor.value.dst_public_key === public_key) {
          chunks.push(cursor.value);
        }
        cursor.continue();
      } else {
        on_success(chunks);
      }
    }
  }
  getUnDeliveredChunksByPublicKey(public_key, on_success) {
    const transaction = this.db.transaction('chunks', 'readonly');
    const chunksObjectStore = transaction.objectStore('chunks');
    const request = chunksObjectStore.openCursor();
    const chunks = [];
    request.onsuccess = event => {
      const cursor = event.target.result;
      if (cursor) {
        if (cursor.value.dst_public_key === public_key && !cursor.value.delivered) {
          chunks.push(cursor.value);
        }
        cursor.continue();
      } else {
        on_success(chunks);
      }
    }
  }
  getChunk(id, index, on_found, on_not_found) {
    const transaction = this.db.transaction('chunks', 'readwrite');
    const chunks = transaction.objectStore('chunks');
    const dr = chunks.get(`${id}-${index}`);
    dr.onsuccess = res => {
      const chunk = res.target.result;
      if (chunk)
        on_found(chunk);
      else
        on_not_found();
    }
    dr.onerror = () => {
      on_not_found();
    }
  }
  setChunkDelivered(id, index, on_success, on_failure) {
    this.getChunk(id, index, chunk => {
      const transaction = this.db.transaction('chunks', 'readwrite');
      const chunks = transaction.objectStore('chunks');
      chunk.delivered = true;
      chunk.delivered_at = Date.now();
      const ur = chunks.put(chunk);
      ur.onsuccess = () => {
        on_success();
      };
      ur.onerror = res => {
        on_failure(res)
      };
    }, () => {
      on_failure();
    })
  }
}