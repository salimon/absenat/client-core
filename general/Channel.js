const NodeRSA = require('node-rsa');
module.exports = class Channel {
  constructor(node, ip, port, dst_public_key) {
    this.socket = new WebSocket(`ws://${ip}:${port}`);
    this.connected = false;
    this.socket.onopen = event => {
      this.connected = true;
      if (!this.is_waiting_for_ack)
        this.sendMessage();
    };

    this.ip = ip;
    this.port = port;
    this.node = node;
    this.event_listeners = {};
    this.once_event_listeners = {};
    this.last_time_connected = 0;
    this.message_queue = []; // {event, content}
    this.is_waiting_for_ack = false;

    this.src_private_key = new NodeRSA(`-----BEGIN PRIVATE KEY-----${node.private_key}-----END PRIVATE KEY-----`);
    this.src_private_key_str = node.private_key;

    this.dst_public_key = new NodeRSA(`-----BEGIN PUBLIC KEY-----${dst_public_key}-----END PUBLIC KEY-----`);
    this.dst_public_key_str = dst_public_key;


    this.socket.onmessage = data => {
      const [action, cipher] = JSON.parse(data.data);
      switch (action) {
        case 'msg':
          const answer = JSON.parse(this.src_private_key.decrypt(cipher, 'utf8').toString());
          const sign = answer.s;
          if (this.dst_public_key) {
            if (!this.dst_public_key.verify({ e: answer.e, c: answer.c }, sign, 'base64', 'base64')) {
              console.log(`Err: invalid message on ${answer.e}`);
              this.sendDirect('err', { c: 100, m: 'invalid signiture' });
              return;
            }
          }
          const event = answer.e;
          const content = answer.c

          if (event === 'err') {
            console.log(`Error: ${content.c}: ${content.m}`);
            return;
          }

          if (this.event_listeners[event]) {
            this.event_listeners[event].forEach(callback => {
              callback(content);
            });
          }
          if (this.once_event_listeners[event]) {
            this.once_event_listeners[event].forEach(callback => {
              callback(content);
              delete this.once_event_listeners[event];
            });
          }
          this.sendAck();
          break;
        case 'error':
          console.log(error);
          break;
        case 'disconnect':
          console.log(e);
          console.log(`channel ${this.nickname} removed - disconnect`);
          removeFromList(this);
          break;
        case 'ack':
          if (this.message_queue.length === 0) {
            this.is_waiting_for_ack = false;
          } else {
            this.sendMessage();
          }
          break;
        default:
          console.log('unhandled data');
      }
    }

    this.sendMessage = () => {
      const message = this.message_queue.shift();
      console.log(message);
      if (message) {
        const { event, content } = message;
        const cipher = this.dst_public_key.encrypt(JSON.stringify({
          e: event,
          c: content,
          s: this.src_private_key.sign({ e: event, c: content }, 'base64', 'base64'),
        }), 'base64');
        this.socket.send(['msg', cipher]);
        this.is_waiting_for_ack = true;
      } else {
        this.is_waiting_for_ack = false;
      }
    };
  }

  sendAck() {
    this.socket.send(['ack']);
  }
  sendDirect(event, content) {
    this.message_queue.push({ event, content });
    if (!this.is_waiting_for_ack && this.connected)
      this.sendMessage();
  }
  on(event, callback) {
    if (!this.event_listeners[event]) {
      this.event_listeners[event] = [];
    }
    this.event_listeners[event].push(callback);
  }
  once(event, callback) {
    if (!this.event_listeners[event]) {
      this.once_event_listeners[event] = [];
    }
    this.once_event_listeners[event].push(callback);
  }
  setNickname(nickname) {
    this.nickname = nickname;
  }
  updateLastTimeConnected() {
    this.last_time_connected = Date.now();
    this.node.db.updateBridgeLastTimeConnected(this.dst_public_key_str, () => { }, () => { });
  }
  saveBridge(on_success, on_failure) {
    this.updateLastTimeConnected();
    this.node.db.createBridge(this.dst_public_key_str, this.nickname, this.ip, this.port, () => {
      console.log('birdge saved in db');
      on_success();
    }, () => {
      console.log("can't save this bridge");
      on_failure();
    });
  }
  startHeartBeat(destroy) {
    // const sendHeartBeat = () => {
    //   if (!this.isAlive()) {
    //     destroy();
    //   } else {
    //     this.sendDirect('heartbeat', { t: Date.now() });
    //     setTimeout(sendHeartBeat, this.node.max_timeout);
    //   }
    // };
    // this.on('heartbeat', () => {
    //   this.updateLastTimeConnected();
    // });
    // sendHeartBeat();
  }
  destroy() {
    this.socket.close();
  }
  isAlive() {
    return this.last_time_connected > Date.now() - (this.node.max_timeout * 2.5)
  }
}