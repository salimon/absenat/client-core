const DB = require('./general/DB');
class Absenat {
  constructor(nickname, public_key, private_key, max_timeout) {
    this.nickname = nickname;
    this.public_key = public_key;
    this.private_key = private_key;
    this.max_timeout = max_timeout;
    this.channels = [];
    this.messages = {};

    this.db = new DB();
    this.db.init(public_key, () => {
      console.log('indexDB initialized.');
      this.on_db_initialized();
    }, () => {
      console.log('failed to initialize indexDB');
      this.on_db_initialization_got_error();
    });

    require('./commands')(this);
    require('./general/Events')(this);
  }
  getChannel(ip, port) {
    for (const i in this.channels) {
      if (this.channels[i].ip === ip && this.channels[i].port === port)
        return this.channels[i];
    }
    return null;
  }
  addChannel(channel) {
    const channels = this.channels;
    channels.push(channel);
    this.channels = channels;
  }
  getBridges(resolve) {
    this.db.getAllBridges(bridges => {
      const result = bridges.map(bridge => {
        const channel = this.getChannel(bridge.ip, bridge.port);
        if (channel) {
          bridge.online = true;
          bridge.channel = channel;
        } else {
          bridge.online = false;
        }
        return bridge;
      });
      resolve(result);
    }, () => {
      resolve([]);
    })
  }
  getContacts(resolve) {
    this.db.getAllContacts(contacts => {
      resolve(contacts);
    }, () => {
      resolve([]);
    });
  }
};

module.exports = Absenat;