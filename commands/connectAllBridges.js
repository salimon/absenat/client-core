module.exports = (me) => {
  me.isConnectedToBridge = (ip, port) => {
    let connected = false;
    for (const i in me.channels) {
      if (me.channels[i].ip === ip && me.channels[i].port === port) connected = true;
    }
    return connected;
  }
  me.connectAllBridges = finished => {
    me.db.getAllBridges(bridges => {
      bridges.forEach(bridge => {
        if (!me.isConnectedToBridge(bridge.ip, bridge.port)) {
          me.connectToBridge(bridge.ip, bridge.port, bridge.public_key);
        }
      });
      finished();
    });
  }
}