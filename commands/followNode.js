module.exports = (me) => {
  me.followNode = (public_key, channel, on_followed, on_not_found) => {
    channel.sendDirect('follow_node', { pk: public_key });
    channel.once('node_followed', contact => { // node followed
      me.db.createContact(contact.pk, contact.n, channel.dst_public_key_str, () => {
        console.log('inserted in db');
      }, () => {
        console.log('error in db');
      })
      on_followed();
    });
    channel.once('node_not_found', () => { // node not found
      on_not_found()
    });
  }
}