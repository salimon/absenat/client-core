module.exports = (Absenat) => {
  require('./connectToBridge')(Absenat);
  require('./connectAllBridges')(Absenat);
  require('./fetchNode')(Absenat);
  require('./followNode')(Absenat)
  require('./sendIndirectMessage')(Absenat);
  require('./sendIndirectMedia')(Absenat);
}
