module.exports = (me) => {
  me.fetchNode = (public_key, on_found, on_not_found) => {
    me.channels.forEach(channel => {
      // step 0: send request to every connected channel
      channel.sendDirect('fetch_node', {pk: public_key});
      // step 1: bridge responses if node not found
      channel.once('node_not_found', () => { // node not found
        on_not_found(channel);
      });
      channel.once('node_found', contact => { // node found
        on_found(channel, contact);
      });
    });
  }  
}