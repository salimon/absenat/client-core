const MessageLib = require('../general/Message');
const { uuid } = require('uuidv4');
module.exports = (me) => {
  /**
   * @author mr-exception
   * @param {string} dst_public_key
   * @param {string} content_type text, image, video, movie, file
   * @param {buffer} content
   * @param {function} on_sent
   * @param {function} on_not_sent
   */
  me.sendIndirectMessage = (dst_public_key, content_type, content, on_sent, on_not_sent, store = true) => {
    me.db.getContact(dst_public_key, contact => {
      contact.bridges.forEach(bridge_public_key => {
        me.db.getBridge(bridge_public_key, bridge => {
          const channel = me.getChannel(bridge.ip, bridge.port);
          if (!channel) {
            console.log(`bridge ${bridge.nickname} is not alive.`);
            on_not_sent();
            return false;
          }
          // prepare message chunks to send
          const chunks = MessageLib.createMessage(me, dst_public_key, content, content_type);
          const id = uuid();
          chunks.forEach((chunk, index) => {
            // send chunk to bridge
            channel.sendDirect('indirect_message', { chunk, index, id, dst_public_key, count: chunks.length, store });
          });
          console.log(`sending all chunks: ${chunks.length}`);
          on_sent();
        }, () => {
          console.log('bridge not found!');
          on_not_sent();
        });
      });
    }, () => {
      console.log('contact not found!');
      on_not_sent();
    });
  }
}