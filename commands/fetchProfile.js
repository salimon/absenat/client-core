const Channel = require('../general/Channel');
module.exports = (me) => {
  me.connectToBridge = (ip, port, bridge_public_key) => {
    const channel = new Channel(me, ip, port, bridge_public_key);
    console.log(`connecting to ${ip}:${port}...`);
    // step 0: send public key
    channel.sendDirect('connect_0', { pk: me.public_key });
    console.log('sent base informations');
    // Step 1: get nickanem
    channel.on('connect_1', (content) => {
      channel.setNickname(content.n);
      channel.sendDirect('connect_2', { n: me.nickname });
      channel.on('connect_3', content => {
        console.log(`bridge message: ${content.m}`);
        channel.saveBridge();
      });
    });
  }
}