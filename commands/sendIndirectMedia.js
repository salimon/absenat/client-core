const MessageLib = require('../general/Message');
const { uuid } = require('uuidv4');
module.exports = (me) => {
  /**
   * @author mr-exception
   * @param {string} dst_public_key
   * @param {Blob} file
   * @param {type} string
   * @param {function} on_sent
   * @param {function} on_not_sent
   */
  me.sendIndirectMediaMessage = (dst_public_key, file, type, on_sent, on_not_sent, store = true) => {
    me.db.getContact(dst_public_key, contact => {
      contact.bridges.forEach(bridge_public_key => {
        me.db.getBridge(bridge_public_key, bridge => {
          const channel = me.getChannel(bridge.ip, bridge.port);
          if (!channel) {
            console.log(`bridge ${bridge.nickname} is not alive.`);
            on_not_sent();
            return false;
          }
          // prepare message chunks to send
          MessageLib.toBase64(file, content => {
            const chunks = MessageLib.createMessage(me, dst_public_key, content, type);
            const id = uuid();
            chunks.forEach((chunk, index) => {
              channel.sendDirect('indirect_message', { chunk, index, id, dst_public_key, count: chunks.length, store });
            });
            console.log(`sending all chunks: ${chunks.length}`);
            on_sent();
          }, () => {
            console.log('error on reading file.');
            on_not_sent();
          })
        }, () => {
          console.log('bridge not found!');
          on_not_sent();
        });
      });
    }, () => {
      console.log('contact not found!');
      on_not_sent();
    });
  }
  me.sendIndirectImageMessage = (dst_public_key, file, on_sent, on_not_sent) => {
    me.sendIndirectMediaMessage(dst_public_key, file, 'image', on_sent, on_not_sent);
  }
  me.sendIndirectAudioMessage = (dst_public_key, file, on_sent, on_not_sent) => {
    me.sendIndirectMediaMessage(dst_public_key, file, 'audio', on_sent, on_not_sent);
  }
  me.sendIndirectFileMessage = (dst_public_key, file, on_sent, on_not_sent) => {
    me.sendIndirectMediaMessage(dst_public_key, file, 'file', on_sent, on_not_sent);
  }
  me.sendIndirectMovieMessage = (dst_public_key, file, on_sent, on_not_sent) => {
    me.sendIndirectMediaMessage(dst_public_key, file, 'movie', on_sent, on_not_sent);
  }
}