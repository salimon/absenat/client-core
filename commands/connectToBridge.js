const Channel = require('../general/Channel');
const MessageLib = require('../general/Message');
module.exports = (me) => {
  /**
   * @author mr-exception
   * @param {string} ip
   * @param {string} port
   * @param {string} bridge_public_key
   */
  me.connectToBridge = (ip, port, bridge_public_key) => {
    let channel = me.getChannel(ip, port);
    if (channel && channel.isAlive()) {
      me.on_connected_to_bridge_before(channel);
      return console.log('connected to channel before!');
    }
    channel = new Channel(me, ip, port, bridge_public_key);

    // set a flag for connection status. if it was false (not connected) after timeout then connection has been failed
    let connected = false;
    setTimeout(() => {
      if (!connected)
        me.on_connection_to_bridge_failed(channel);
    }, me.max_timeout);

    me.addChannel(channel);
    console.log(`connecting to ${ip}:${port}...`);
    // step 0: send public key
    channel.sendDirect('connect_0', { pk: me.public_key });
    console.log('sent base informations');
    // Step 1: get nickanem
    channel.on('connect_1', (content) => {
      channel.setNickname(content.n);
      channel.sendDirect('connect_2', { n: me.nickname });
      channel.on('connect_3', content => {
        console.log(`bridge message: ${content.m}`);
        // set connection flag to true and call the success callback
        // save bridge in database
        channel.saveBridge(() => {
          connected = true;
          me.on_connected_to_bridge(channel);
          // starts heartbeat operation for this channel
          channel.startHeartBeat(() => { // destroy the channel
            channel.destroy();
            me.channels = me.channels.filter(ch => ((ch.ip === channel.ip && ch.port === channel.port) ? null : ch));
            // delete channel;
            console.log('channel removed');
          });
        }, () => { });
      });
    });
    channel.on('data_chunk', content => {
      const { id, index, dst_public_key, chunk, count } = content;
      me.db.storeChunk(id, index, chunk, count, dst_public_key, () => {
        console.log(`stored: ${id}-${index}`);
      }, () => {
        console.log('failure');
      });
    });
    /**
     * when bridge sends a chunk content
     */
    channel.on('chunk', content => {
      console.log(content);
      const { id, index, chunk, count } = content;
      const msg = me.messages[id] || { chunks: [], id, count };
      msg.chunks[index] = chunk;
      me.messages[id] = msg;
      if (msg.count === msg.chunks.length) {
        const message = MessageLib.convertChunksToMessage(me.private_key, msg.chunks);
        if (!message)
          return console.log('Err: invalid message.');
        const { body, type, sender_public_key } = message;
        me.db.getContact(sender_public_key, contact => {
          me.on_message_recieved(type, body, contact); // if message recieved from a known node
        }, () => {
          me.on_anonymouse_message_recieved(type, body, sender_public_key); // if message recieved from a unknown node
        });
      }
    });
    channel.on('is_online', content => {
      const { public_key } = content;
      me.db.getUnDeliveredChunksByPublicKey(public_key, data => {
        console.log(`finished searching for chunks, found ${data.length} chunks to send.`);
        data.forEach(item => {
          const { chunk, index, id, dst_public_key, count } = item;
          channel.sendDirect('indirect_message', { chunk, index, id, dst_public_key, count, store: false });
          me.db.setChunkDelivered(id, index, () => {
            console.log(`delivered chunk ${index}-${id}`);
          }, () => {
            console.log(`can't find chunk in db`);
          });
        });
      });
    });
  }
}